﻿using System.Collections.Generic;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class CanStop
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();
            var fb = A.Fake<Core.BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flow.Blocks).Returns(new List<Core.BlockInstance> { fb });

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.CanStop()).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);
        }

        [TestMethod]
        public void WhenActiveBlockIsNotNull_ResultShouldBeTrue()
        {
            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeTrue();
        }
        
        [TestMethod]
        public void WhenFlowIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.Flow).Returns(null);

            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotDefined);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(null);

            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsEmpty_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>());

            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenActiveBlockIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(null);

            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotActive);
        }

        [TestMethod]
        public void WhenBlockPropertyOfActiveBlockIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block).Returns(null);

            //Act
            var result = flowRunner.CanStop();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotActive);
        }
    }
}