﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Resolvers;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class Continue
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();
            
            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.Continue()).CallsBaseMethod();
            A.CallTo(() => flowRunner.CanContinue()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.Flow).Returns(flow);

            var fb = A.Fake<BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => fb.Block.Clone()).Returns(A.Fake<IBlock>());
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(fb);

            var nextBlockResult = A.Fake<IResult<IBlock>>();
            A.CallTo(() => nextBlockResult.IsOk).Returns(true);
            var br = A.Fake<IBlockResolver>();
            A.CallTo(() => br.ResolveNextBlock(A<IBlock>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).Returns(nextBlockResult);
            A.CallTo(() => flowRunner.BlockResolver).Returns(br);
        }

        [TestMethod]
        public void WhenCalled_CanContinueMustHaveBeenCalledOnce()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.CanContinue()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_ACallToDeactivateMustHaveHappenedOnActiveBlock()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_ACallToSetActiveBlockMustHaveHappened()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.SetActiveBlock(A<Core.BlockInstance>._)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_ACallToSatisfyRequirementsMustHaveHappenedOnActiveBlock()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.SatisfyRequirements(flowRunner.ActiveBlockInstance)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_ACallToActivateMustHaveHappenedOnActiveBlock()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Activate()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_OldActiveBlockInstanceMustHaveBeenAddedAsLastItemInHistoryCollection()
        {
            //Arrange
            var guid = Guid.NewGuid();
            var blockKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => blockKey.Uuid).Returns(guid);
            A.CallTo(() => flowRunner.ActiveBlockInstance.Key).Returns(blockKey);

            //Act
            flowRunner.Continue();

            //Assert
            flowRunner.History.Last().Key.ShouldBe(blockKey);
        }

        [TestMethod]
        public void WhenCanContinueReturnsTrue_ACallToResolveNextBlockMethodOnBlockResolverMustHaveHappenedOnActiveBlock()
        {
            //Act
            flowRunner.Continue();

            //Assert
            A.CallTo(() => flowRunner.BlockResolver.ResolveNextBlock(flowRunner.ActiveBlockInstance.Block, A<IEnumerable<IConnection<IBlock>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }
        
        [TestMethod]
        public void WhenResultOfCompleteActionIsSuccessFul_ResultValueShouldBeSetToFlow()
        {
            //Arrange
            A.CallTo(() => flowRunner.CanContinue()).Returns(Result<bool>.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Deactivate()).Returns(Result<IBlock>.Ok());
            A.CallTo(() => flowRunner.BlockResolver.ResolveNextBlock(A<IBlock>.Ignored, A<List<IConnection<IBlock>>>.Ignored)).Returns(Result<IBlock>.Ok());
            A.CallTo(() => flowRunner.SetActiveBlock(A<BlockInstance>.Ignored)).Returns(Result<IFlow>.Ok());
            A.CallTo(() => flowRunner.SatisfyRequirements(A<BlockInstance>.Ignored)).Returns(Result<IFlow>.Ok());
            A.CallTo(() => flowRunner.ActiveBlockInstance.Block.Activate()).Returns(Result<IBlock>.Ok());

            //Act
            var result = flowRunner.Continue();
            
            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flowRunner.Flow);
        }
    }
}