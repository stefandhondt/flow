﻿using System;
using System.Collections.Generic;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class CanStart
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();
            var fb = A.Fake<Core.BlockInstance>();
            A.CallTo(() => fb.Block).Returns(A.Fake<IBlock>());
            A.CallTo(() => flow.Blocks).Returns(new List<Core.BlockInstance> { fb });
            A.CallTo(() => flow.GetInitialBlock()).Returns(Result<BlockInstance>.Ok(fb));

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.CanStart()).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(null);
        }
        
        [TestMethod]
        public void WhenFlowIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.Flow).Returns(null);

            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowNotDefined);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNotEmptyAndBlockPropertyOfInitialBlockIsNotNullAndActiveBlockIsNull_ResultShouldBeTrue()
        {
            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeTrue();
        }

        [TestMethod]
        public void WhenBlocksCollectionIsNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(null);

            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenBlocksCollectionIsEmpty_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>());

            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockCollectionEmpty);
        }

        [TestMethod]
        public void WhenInitialBlockIsNull_ResultShouldBeFalse()
        {
            //Arrange
            var invalidResult = Result<BlockInstance>.Ok().AddIssue(new IssueCode(Guid.NewGuid().ToString(), 0));
            A.CallTo(() => flow.GetInitialBlock()).Returns(invalidResult);

            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeFalse();
        }

        [TestMethod]
        public void WhenActiveBlockIsNotNull_ResultShouldBeFalse()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(A.Fake<Core.BlockInstance>());

            //Act
            var result = flowRunner.CanStart();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowActive);
        }
    }
}