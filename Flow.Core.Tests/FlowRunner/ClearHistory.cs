using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class ClearHistory
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;
        
        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.ClearHistory()).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenActiveBlockInstanceIsNotNull_ResultShouldBeInvalidWithIssueCodeFlowActive()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(A.Fake<Core.BlockInstance>());

            //Act
            var result = flowRunner.ClearHistory();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.FlowRunner.FlowActive);
        }
        
        [TestMethod]
        public void WhenActiveBlockInstanceIsNull_ResultShouldBeValid()
        {
            //Arrange
            A.CallTo(() => flowRunner.ActiveBlockInstance).Returns(null);

            //Act
            var result = flowRunner.ClearHistory();

            //Assert
            result.IsOk.ShouldBeTrue();
        }
    }
}