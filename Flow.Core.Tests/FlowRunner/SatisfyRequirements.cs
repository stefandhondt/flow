using System.Collections.Generic;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.FlowRunner
{
    [TestClass]
    public class SatisfyRequirements
    {
        private Core.FlowRunner flowRunner;
        private Core.IFlow flow;
        private Core.BlockInstance blockInstance;
        private Core.IBlock blockInstanceBlock;
        
        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.IFlow>();

            flowRunner = A.Fake<Core.FlowRunner>();
            A.CallTo(() => flowRunner.SatisfyRequirements(A<BlockInstance>.Ignored)).CallsBaseMethod();
            A.CallTo(() => flowRunner.Flow).Returns(flow);

            blockInstanceBlock = A.Fake<IBlock>();
            blockInstance = A.Fake<BlockInstance>();
            A.CallTo(() => blockInstance.Block).Returns(blockInstanceBlock);
        }

        [TestMethod]
        public void WhenBlockInstanceIsNull_ResultShouldBeInvalid()
        {
            //Arrange
            blockInstance = null;
            
            //Act
            var result = flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }
        
        [TestMethod]
        public void WhenBlockOfBlockInstanceIsNull_ResultShouldBeInvalid()
        {
            //Arrange
            A.CallTo(() => blockInstance.Block).Returns(null);
            
            //Act
            var result = flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }
        
        [TestMethod]
        public void WhenRequirementsCollectionOfBlockOfBlockInstanceIsNull_ResultShouldBeValidAndValueShouldBeSetToFlow()
        {
            //Arrange
            A.CallTo(() => blockInstance.Block.Requirements).Returns(null);
            
            //Act
            var result = flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flowRunner.Flow);
        }
        
        [TestMethod]
        public void WhenRequirementsCollectionOfBlockOfBlockInstanceIsEmpty_ResultShouldBeValidAndValueShouldBeSetToFlow()
        {
            //Arrange
            A.CallTo(() => blockInstance.Block.Requirements).Returns(A.Fake<IReadOnlyCollection<IRequirement>>());
            
            //Act
            var result = flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flowRunner.Flow);
        }
        
        [TestMethod]
        public void WhenRequirementsCollectionOfBlockOfBlockInstanceIsNotEmptyAndGetAccumulatedContributionsReturnsEmptyCollection_ResultShouldBeInvalid()
        {
            //Arrange
            var contributions = new List<IEnumerable<IContribution>>();
            A.CallTo(() => flowRunner.GetAccumulatedContributions()).Returns(contributions);
            var requirements = new List<IRequirement>
            {
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>()
            };
            A.CallTo(() => blockInstance.Block.Requirements).Returns(requirements.AsReadOnly());
            
            //Act
            var result = flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            result.IsOk.ShouldBeFalse();
        }
        
        [TestMethod]
        public void WhenRequirementsCollectionOfBlockOfBlockInstanceIsNotEmpty_RequirementResolverSatisfyRequirementMustHaveBeenCalledForEachRequirement()
        {
            //Arrange
            var contributions = new List<IEnumerable<IContribution>>();
            A.CallTo(() => flowRunner.GetAccumulatedContributions()).Returns(contributions);
            var requirements = new List<IRequirement>
            {
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>(),
                A.Fake<IRequirement>()
            };
            A.CallTo(() => blockInstance.Block.Requirements).Returns(requirements.AsReadOnly());
            
            //Act
            flowRunner.SatisfyRequirements(blockInstance);

            //Assert
            foreach(var requirement in requirements)
            {
                A.CallTo(() => flowRunner.RequirementResolver.SatisfyRequirement(requirement, blockInstance.Block, contributions)).MustHaveHappened(Repeated.Exactly.Once);
            }
        }
    }
}