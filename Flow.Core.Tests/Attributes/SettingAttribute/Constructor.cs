using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Attributes.SettingAttribute
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_PropertiesShouldMatchParameters()
        {
            //Arrange
            var nameCollection = new[]
            {
                Guid.NewGuid().ToString(),
                null,
                string.Empty
            };

            var defaultValueCollection = new[]
            {
                Guid.NewGuid().ToString(),
                null,
                string.Empty
            };
            
            var descriptionCollection = new[]
            {
                Guid.NewGuid().ToString(),
                null,
                string.Empty
            };

            foreach(var name in nameCollection)
            {
                foreach(var value in defaultValueCollection)
                {
                    foreach(var description in descriptionCollection)
                    {
                        //Act
                        var setting = new Core.Attributes.SettingAttribute(name, value, description);

                        //Assert
                        setting.Name.ShouldBe(name);
                        setting.DefaultValue.ShouldBe(value);
                        setting.Description.ShouldBe(description);
                    }
                }
            }
        }
        
        [TestMethod]
        public void WhenDefaultValueParameterIsNotProvided_DefaultValueShouldBeNull()
        {
            //Act
            var setting = new Core.Attributes.SettingAttribute(Guid.NewGuid().ToString(), description: Guid.NewGuid().ToString());

            //Assert
            setting.DefaultValue.ShouldBeNull();
        }
        
        [TestMethod]
        public void WhenDescriptionParameterIsNotProvided_DescriptionShouldBeNull()
        {
            //Act
            var setting = new Core.Attributes.SettingAttribute(Guid.NewGuid().ToString(), defaultValue: Guid.NewGuid().ToString());

            //Assert
            setting.Description.ShouldBeNull();
        }
    }
}