﻿using System.Linq;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockFactory
{
    [TestClass]
    public class GetAvailableBlockTypeNames
    {
        private Core.BlockFactory blockFactory;

        [TestInitialize]
        public void Init()
        {
            blockFactory = A.Fake<Core.BlockFactory>();
            A.CallTo(() => blockFactory.GetAvailableBlockTypeNames()).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenCalled_ResultCollectionShouldMatchFullNamePropertyOfAllBlockTypes()
        {
            //Arrange
            var type1 = typeof(int);
            var type2 = typeof(decimal);
            var type3 = typeof(float);
            var types = new[] { type1, type2, type3 };
            A.CallTo(() => blockFactory.BlockTypes).Returns(types);

            //Act
            var result = blockFactory.GetAvailableBlockTypeNames();

            //Assert
            result.Count().ShouldBe(types.Length);

            foreach(var type in types)
            {
                result.Contains(type.FullName).ShouldBeTrue();
            }
        }
    }
}