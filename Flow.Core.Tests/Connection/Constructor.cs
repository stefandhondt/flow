﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Connection
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_PropertiesShouldMatchParameters()
        {
            //Arrange
            var from = Guid.NewGuid();
            var to = Guid.NewGuid();
            var continuationValue = DateTime.Now.Millisecond;

            //Act
            var connection = new Core.Connection<Guid>(from, to, continuationValue);

            //Assert
            connection.From.ShouldBe(from);
            connection.To.ShouldBe(to);
            connection.ContinuationValue.ShouldBe(continuationValue);
        }
    }
}