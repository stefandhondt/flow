using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_NameShouldMatchParameter()
        {
            //Arrange
            var name = Guid.NewGuid().ToString();
            
            //Act
            var block = new TestBlock(name, null);
            
            //Assert
            Debug.WriteLine(block);
            block.Name.ShouldBe(name);
        }
        
        [TestMethod]
        public void WhenCalled_DescriptionShouldMatchParameter()
        {
            //Arrange
            var description = Guid.NewGuid().ToString();
            
            //Act
            var block = new TestBlock(null, description);
            
            //Assert
            Debug.WriteLine(block);
            block.Description.ShouldBe(description);
        }
    }
}