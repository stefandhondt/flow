using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Validate
    {
        private readonly TestBlock block = new TestBlock();
        
        [TestMethod]
        public void WhenCalled_ValidatedEventMustBeFired()
        {
            //Arrange
            var eventHandlerCalls = 0;
            block.Validated += (sender, args) => { eventHandlerCalls += 1; };

            //Act
            block.Validate();

            //Assert
            eventHandlerCalls.ShouldBe(1);
        }
    }
}