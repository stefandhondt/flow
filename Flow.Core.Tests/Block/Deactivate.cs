using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Deactivate
    {
        private readonly TestBlock block = new TestBlock();
        
        [TestMethod]
        public void WhenCalled_DeactivatedEventMustBeFired()
        {
            //Arrange
            var eventHandlerCalls = 0;
            block.Deactivated += (sender, args) => { eventHandlerCalls += 1; };

            //Act
            block.Deactivate();

            //Assert
            eventHandlerCalls.ShouldBe(1);
        }
    }
}