using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Activate
    {
        private readonly TestBlock block = new TestBlock();
        
        [TestMethod]
        public void WhenCalled_ActivatedEventMustBeFired()
        {
            //Arrange
            var eventHandlerCalls = 0;
            block.Activated += (sender, args) => { eventHandlerCalls += 1; };

            //Act
            block.Activate();

            //Assert
            eventHandlerCalls.ShouldBe(1);
        }
    }
}