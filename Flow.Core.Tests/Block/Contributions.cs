using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Contributions
    {
        private TestBlock block;
        
        [TestInitialize]
        public void Init()
        {
            block = new TestBlock();
        }
        
        [TestMethod]
        public void WhenCalled_ContributionsCollectionShouldBeInitialised()
        {
            //Act
            var contributions = block.Contributions;

            //Assert
            Debug.WriteLine(block);
            contributions.Count.ShouldBe(2);
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber));
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.PostalCode));
        }
        
        [TestMethod]
        public void WhenCalledAfterUpdatingARequirementValue_RequirementsCollectionShouldReflectUpdate()
        {
            //Arrange
            var contributions = block.Contributions;
            var newPostalCode = Guid.NewGuid().ToString();
            var newStreetNumber = newPostalCode.Length;
            
            //Assert
            Debug.WriteLine("Pre");
            Debug.WriteLine(block);
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber) && (int)r.Value == default(int));
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.PostalCode) && (string)r.Value == default(string));

            //Act
            block.StreetNumber = newStreetNumber;
            block.PostalCode = newPostalCode;
            contributions = block.Contributions;

            //Assert
            Debug.WriteLine("Post");
            Debug.WriteLine(block);
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.StreetNumber) && (int)r.Value == newStreetNumber);
            contributions.ShouldContain(r => r.Property.Name == nameof(TestBlock.PostalCode) && (string)r.Value == newPostalCode);
        }
    }
}