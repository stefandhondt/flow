using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Block
{
    [TestClass]
    public class Settings
    {
        private TestBlock block;
        
        [TestInitialize]
        public void Init()
        {
            block = new TestBlock();
        }
        
        [TestMethod]
        public void WhenCalled_SettingsCollectionShouldBeInitialised()
        {
            //Act
            var settings = block.Settings;

            //Assert
            Debug.WriteLine(block);
            settings.Count.ShouldBe(3);
            settings.ShouldContain(s => s.Key == TestBlock.SettingValues.Setting1Name && s.Value == s.DefaultValue && s.DefaultValue == TestBlock.SettingValues.Setting1DefaultValue && s.Description == TestBlock.SettingValues.Setting1Description );
            settings.ShouldContain(s => s.Key == TestBlock.SettingValues.Setting2Name && s.Value == s.DefaultValue && s.DefaultValue == TestBlock.SettingValues.Setting2DefaultValue && s.Description == null );
            settings.ShouldContain(s => s.Key == TestBlock.SettingValues.Setting3Name && s.Value == s.DefaultValue && s.DefaultValue == null && s.Description == null );
        }
    }
}