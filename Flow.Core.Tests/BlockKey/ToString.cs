﻿using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.BlockKey
{
    [TestClass]
    public class ToString
    {
        private Core.BlockKey lhs;

        [TestInitialize]
        public void Init()
        {
            lhs = A.Fake<Core.BlockKey>();
            A.CallTo(() => lhs.ToString()).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenCalled_UuidPropertyValueShouldBePartOfResult()
        {
            //Arrange
            var guid = Guid.NewGuid();
            A.CallTo(() => lhs.Uuid).Returns(guid);

            //Act
            var result = lhs.ToString();

            //Assert
            result.Contains(lhs.Uuid.ToString()).ShouldBeTrue();
        }
    }
}