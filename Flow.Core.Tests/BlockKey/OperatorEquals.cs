﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flow.Core.Tests.BlockKey
{
    [TestClass]
    public class OperatorEquals
    {
        private Core.BlockKey lhs;
        private Core.BlockKey rhs;

        [TestInitialize]
        public void Init()
        {
            lhs = A.Fake<Core.BlockKey>();
            rhs = A.Fake<Core.BlockKey>();
        }

        [TestMethod]
        public void WhenLhsIsFirstArgument_ACallToEqualsMethodMustHaveHappenedOnce()
        {
            //Act
            var result = lhs == rhs;

            //Assert
            A.CallTo(() => lhs.Equals(rhs)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestMethod]
        public void WhenRhsIsFirstArgument_ACallToEqualsMethodMustHaveHappenedOnce()
        {
            //Act
            var result = rhs == lhs;

            //Assert
            A.CallTo(() => rhs.Equals(lhs)).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}