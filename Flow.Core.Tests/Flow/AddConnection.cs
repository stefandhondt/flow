﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class AddConnection
    {
        private Core.Flow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.AddConnection(A<Core.BlockKey>.Ignored, A<Core.BlockKey>.Ignored, A<object>.Ignored)).CallsBaseMethod();

            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                CreateBlock(),
                CreateBlock()
            });
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>
            {
                CreateBlockConnection(flow.Blocks[0].Key, flow.Blocks[1].Key),
                CreateBlockConnection(flow.Blocks[1].Key, flow.Blocks[2].Key)
            });
        }

        [TestMethod]
        public void WhenFromKeyIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            var toKey = A.Fake<Core.BlockKey>();

            //Act
            var result = flow.AddConnection(null, toKey, null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull
                                             && i.Parameters.Any(p => p.Key == IssueParameters.General.Argument.Key));
        }

        [TestMethod]
        public void WhenToKeyIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Arrange
            var fromKey = A.Fake<Core.BlockKey>();

            //Act
            var result = flow.AddConnection(fromKey, null, null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull
                                             && i.Parameters.Any(p => p.Key == IssueParameters.General.Argument.Key));
        }

        [TestMethod]
        public void WhenConnectionsCollectionAlreadyContainsConnectionWithSameFromAndTo_ResultShouldBeInvalidWithIssueCodeFlowDuplicateConnection()
        {
            //Arrange
            var fromKey = flow.Connections.First().From;
            var toKey = flow.Connections.First().To;

            //Act
            var result = flow.AddConnection(fromKey, toKey, null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.DuplicateConnection
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.From.Key
                                                                      && (p.Value as Core.BlockKey) == fromKey)
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.To.Key
                                                                      && (p.Value as Core.BlockKey) == toKey));
        }

        [TestMethod]
        public void WhenBlocksCollectionDoesntContainBlockWithKeyEqualToFrom_ResultShouldBeInvalidWithIssueCodeFlowBlockNotFoundInCollection()
        {
            //Arrange
            var fromKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => fromKey.Uuid).Returns(Guid.NewGuid());
            var toKey = flow.Connections.First().To;

            //Act
            var result = flow.AddConnection(fromKey, toKey, null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockNotFoundInCollection
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.From.Key
                                                                      && (p.Value as Core.BlockKey) == fromKey));
        }

        [TestMethod]
        public void WhenBlocksCollectionDoesntContainBlockWithKeyEqualToTo_ResultShouldBeInvalidWithIssueCodeFlowBlockNotFoundInCollection()
        {
            //Arrange
            var fromKey = flow.Connections.First().From;
            var toKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => toKey.Uuid).Returns(Guid.NewGuid());

            //Act
            var result = flow.AddConnection(fromKey, toKey, null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockNotFoundInCollection
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.To.Key
                                                                      && (p.Value as Core.BlockKey) == toKey));
        }

        [TestMethod]
        public void WhenBlocksCollectionContainsBlocksWithKeyEqualToFromAndToAndConnectionDoesntExistYet_ResultShouldBeValidAndConnectionsCollectionSizeIncreasedBy1()
        {
            //Arrange
            var fromKey = flow.Blocks.First().Key;
            var toKey = flow.Blocks.Last().Key;

            //Assert
            var preCount = flow.Connections.Count;

            //Act
            var result = flow.AddConnection(fromKey, toKey, null);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Connections.Count.ShouldBe(preCount + 1);
        }

        private BlockInstance CreateBlock()
        {
            var bk = A.Fake<Core.BlockKey>();
            A.CallTo(() => bk.Uuid).Returns(Guid.NewGuid());
            var block = A.Fake<Core.Block>();
            var flowBlock = A.Fake<BlockInstance>();
            A.CallTo(() => flowBlock.Block).Returns(block);
            A.CallTo(() => flowBlock.Key).Returns(bk);

            return flowBlock;
        }

        private IConnection<Core.BlockKey> CreateBlockConnection(Core.BlockKey from, Core.BlockKey to)
        {
            var bkc = A.Fake<BlockKeyConnection>();
            A.CallTo(() => bkc.From).Returns(from);
            A.CallTo(() => bkc.To).Returns(to);

            return bkc;
        }
    }
}