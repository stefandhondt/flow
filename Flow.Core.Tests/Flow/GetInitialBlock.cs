﻿using System.Collections.Generic;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class GetInitialBlock
    {
        private Core.Flow flow;
        private Core.BlockInstance blockInstance;

        [TestInitialize]
        public void Init()
        {
            blockInstance = A.Fake<Core.BlockInstance>();
            A.CallTo(() => blockInstance.Block).Returns(A.Fake<IBlock>());
            
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.GetInitialBlock()).CallsBaseMethod();
            A.CallTo(() => flow.Blocks).Returns(new List<Core.BlockInstance> { blockInstance });
        }

        [TestMethod]
        public void WhenCalled_ACallToBlockResolverResolveInitialBlockMustHaveHappened()
        {
            //Act
            flow.GetInitialBlock();

            //Assert
            A.CallTo(() => flow.BlockResolver.ResolveInitialBlock(A<IEnumerable<IBlock>>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).MustHaveHappened(Repeated.Exactly.Once);
        }
        
        [TestMethod]
        public void WhenCalledAndResultOfBlockResolverGetInitialBlockIsInvalid_ResultShouldBeInvalidAndValueShouldBeNull()
        {
            //Act
            var result = flow.GetInitialBlock();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
        }

        [TestMethod]
        public void WhenInitialBlockResolvedByBlockResolverWasNotFoundInFlowBlocksCollection_ResultShouldBeInvalidWithIssueCodeBlockNotFoundInCollection()
        {
            //Arrange
            var blockResolverBlock = A.Fake<IBlock>();
            var blockResolverResult = A.Fake<IResult<IBlock>>();
            A.CallTo(() => blockResolverResult.IsOk).Returns(true);
            A.CallTo(() => blockResolverResult.Value).Returns(blockResolverBlock);
            A.CallTo(() => flow.BlockResolver.ResolveInitialBlock(A<IEnumerable<IBlock>>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).Returns(blockResolverResult);

            //Act
            var result = flow.GetInitialBlock();

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockNotFoundInCollection);
        }
        
        [TestMethod]
        public void WhenInitialBlockResolvedByBlockResolverWasFoundInFlowBlocksCollection_ResultShouldBeValid()
        {
            //Arrange
            var blockResolverResult = A.Fake<IResult<IBlock>>();
            A.CallTo(() => blockResolverResult.IsOk).Returns(true);
            A.CallTo(() => blockResolverResult.Value).Returns(blockInstance.Block);
            A.CallTo(() => flow.BlockResolver.ResolveInitialBlock(A<IEnumerable<IBlock>>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).Returns(blockResolverResult);

            //Act
            var result = flow.GetInitialBlock();

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blockInstance);
        }
    }
}