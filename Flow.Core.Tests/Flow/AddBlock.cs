﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class AddBlock
    {
        private Core.Flow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.AddBlock(A<IBlock>.Ignored)).CallsBaseMethod();
            A.CallTo(() => flow.AddBlock(A<Core.BlockKey>.Ignored, A<IBlock>.Ignored)).CallsBaseMethod();

            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                CreateBlock(),
                CreateBlock()
            });
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>
            {
                CreateBlockConnection(flow.Blocks[0].Key, flow.Blocks[1].Key),
                CreateBlockConnection(flow.Blocks[1].Key, flow.Blocks[2].Key)
            });
        }

        [TestMethod]
        public void WhenBlockKeyIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Act
            var result = flow.AddBlock(null, A.Fake<IBlock>());

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void WhenBlockIsNull_ResultShouldBeInvalidWithIssueCodeArgumentNull()
        {
            //Act
            var result = flow.AddBlock(A.Fake<Core.BlockKey>(), null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.General.ArgumentNull);
        }

        [TestMethod]
        public void WhenProvidedBlockKeyMatchesExistingBlockKeyInBlocksCollection_ResultShouldBeInvalidWithIssueCodeBlockIdentifiersNotUnique()
        {
            //Arrange
            var duplicateBlockKey = flow.Blocks.Last().Key;

            //Act
            var result = flow.AddBlock(duplicateBlockKey, A.Fake<IBlock>());

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Value.ShouldBeNull();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.Flow.BlockIdentifierNotUnique
                                             && i.Parameters.Any(p => p.Key == IssueParameters.Flow.Identifier.Key
                                                                      && (p.Value as Core.BlockKey) == duplicateBlockKey));
        }

        [TestMethod]
        public void WhenBlocksCollectionsDoesntContainBlockWithKeyMatchingProvidedBlockKey_ResultShouldBeValidAndBlocksCollectionSizeIncreasedBy1()
        {
            //Arrange
            var blockKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => blockKey.Uuid).Returns(Guid.NewGuid());

            //Assert
            var preCount = flow.Blocks.Count;

            //Act
            var result = flow.AddBlock(blockKey, A.Fake<IBlock>());

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Blocks.Count.ShouldBe(preCount + 1);
        }
        
        [TestMethod]
        public void WhenOverloadWith1ParameterIsCalled_OverloadWith2ParametersShouldBeCalled()
        {
            //Arrange
            var block = A.Fake<IBlock>();

            //Act
            var result = flow.AddBlock(block);

            //Assert
            result.IsOk.ShouldBeTrue();
            A.CallTo(() => flow.AddBlock(A<Core.BlockKey>._, block)).MustHaveHappened(Repeated.Exactly.Once);
        }

        private BlockInstance CreateBlock()
        {
            var bk = A.Fake<Core.BlockKey>();
            A.CallTo(() => bk.Uuid).Returns(Guid.NewGuid());
            var block = A.Fake<Core.Block>();
            var flowBlock = A.Fake<BlockInstance>();
            A.CallTo(() => flowBlock.Block).Returns(block);
            A.CallTo(() => flowBlock.Key).Returns(bk);

            return flowBlock;
        }

        private IConnection<Core.BlockKey> CreateBlockConnection(Core.BlockKey from, Core.BlockKey to)
        {
            var bkc = A.Fake<BlockKeyConnection>();
            A.CallTo(() => bkc.From).Returns(from);
            A.CallTo(() => bkc.To).Returns(to);

            return bkc;
        }
    }
}