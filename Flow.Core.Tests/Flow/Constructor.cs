﻿using FakeItEasy;
using Flow.Core.Resolvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class Constructor
    {
        private IBlockResolver blockResolver;
        private IRequirementResolver requirementResolver;

        public void Init()
        {
            blockResolver = A.Fake<IBlockResolver>();
            requirementResolver = A.Fake<IRequirementResolver>();
        }

        [TestMethod]
        public void WhenCalled_ConnectionsCollectionShouldBeEmpty()
        {
            //Act
            var flow = new Core.Flow(blockResolver, requirementResolver, null);
            
            //Assert
            flow.Connections.ShouldBeEmpty();
        }

        [TestMethod]
        public void WhenCalled_BlocksCollectionShouldBeEmpty()
        {
            //Act
            var flow = new Core.Flow(blockResolver, requirementResolver, null);

            //Assert
            flow.Blocks.ShouldBeEmpty();
        }
        
        [TestMethod]
        public void WhenCalled_BlockResolverShouldMatchParameter()
        {
            //Act
            var flow = new Core.Flow(blockResolver, requirementResolver, null);

            //Assert
            flow.BlockResolver.ShouldBe(blockResolver);
        }
        
        [TestMethod]
        public void WhenCalled_RequirementResolverShouldMatchParameter()
        {
            //Act
            var flow = new Core.Flow(blockResolver, requirementResolver, null);

            //Assert
            flow.RequirementResolver.ShouldBe(requirementResolver);
        }
    }
}