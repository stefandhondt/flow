﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class RemoveBlock
    {
        private Core.Flow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.RemoveBlock(A<Core.BlockKey>.Ignored)).CallsBaseMethod();

            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                CreateBlock(),
                CreateBlock()
            });
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>
            {
                CreateBlockConnection(flow.Blocks[0].Key, flow.Blocks[1].Key),
                CreateBlockConnection(flow.Blocks[1].Key, flow.Blocks[2].Key)
            });
        }

        [TestMethod]
        public void WhenBlockWasNotInBlocksCollection_ResultShouldBeValidAndBlocksCollectionSizeRemainsTheSame()
        {
            //Arrange
            var nonExistingKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => nonExistingKey.Uuid).Returns(Guid.NewGuid());

            //Assert
            var preCount = flow.Blocks.Count;

            //Act
            var result = flow.RemoveBlock(nonExistingKey);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Blocks.Count.ShouldBe(preCount);
        }

        [TestMethod]
        public void WhenBlockWasInBlocksCollection_ResultShouldBeValidAndBlocksCollectionSizeDecreasedBy1()
        {
            //Arrange
            var existingKey = flow.Blocks.First().Key;

            //Assert
            var preCount = flow.Blocks.Count;

            //Act
            var result = flow.RemoveBlock(existingKey);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Blocks.Count.ShouldBe(preCount - 1);
        }

        private BlockInstance CreateBlock()
        {
            var bk = A.Fake<Core.BlockKey>();
            A.CallTo(() => bk.Uuid).Returns(Guid.NewGuid());
            var block = A.Fake<Core.Block>();
            var flowBlock = A.Fake<BlockInstance>();
            A.CallTo(() => flowBlock.Block).Returns(block);
            A.CallTo(() => flowBlock.Key).Returns(bk);

            return flowBlock;
        }

        private IConnection<Core.BlockKey> CreateBlockConnection(Core.BlockKey from, Core.BlockKey to)
        {
            var bkc = A.Fake<BlockKeyConnection>();
            A.CallTo(() => bkc.From).Returns(from);
            A.CallTo(() => bkc.To).Returns(to);

            return bkc;
        }
    }
}