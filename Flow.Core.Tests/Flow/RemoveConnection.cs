﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Flow
{
    [TestClass]
    public class RemoveConnection
    {
        private Core.Flow flow;

        [TestInitialize]
        public void Init()
        {
            flow = A.Fake<Core.Flow>();
            A.CallTo(() => flow.RemoveConnection(A<Core.BlockKey>.Ignored, A<Core.BlockKey>.Ignored)).CallsBaseMethod();

            A.CallTo(() => flow.Blocks).Returns(new List<BlockInstance>
            {
                CreateBlock(),
                CreateBlock(),
                CreateBlock()
            });
            A.CallTo(() => flow.Connections).Returns(new List<IConnection<Core.BlockKey>>
            {
                CreateBlockConnection(flow.Blocks[0].Key, flow.Blocks[1].Key),
                CreateBlockConnection(flow.Blocks[1].Key, flow.Blocks[2].Key)
            });
        }

        [TestMethod]
        public void WhenFromAndToDontMatchAnyExistingConnections_ResultShouldBeValidAndConnectionsCollectionSizeRemainsTheSame()
        {
            //Arrange
            var fromKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => fromKey.Uuid).Returns(Guid.NewGuid());
            var toKey = A.Fake<Core.BlockKey>();
            A.CallTo(() => toKey.Uuid).Returns(Guid.NewGuid());

            //Assert
            var preCount = flow.Connections.Count;

            //Act
            var result = flow.RemoveConnection(fromKey, toKey);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Connections.Count.ShouldBe(preCount);
        }

        [TestMethod]
        public void WhenFromAndToDontMatchAnyExistingConnections_ResultShouldBeValidAndConnectionsCollectionSizeDecreasedBy1()
        {
            //Arrange
            var fromKey = flow.Connections.First().From;
            var toKey = flow.Connections.First().To;

            //Assert
            var preCount = flow.Connections.Count;

            //Act
            var result = flow.RemoveConnection(fromKey, toKey);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Connections.Count.ShouldBe(preCount - 1);
        }

        [TestMethod]
        public void WhenFromAndToAreReversedValuesOfExistingConnection_ResultShouldBeValidAndConnectionsCollectionSizeRemainsTheSame()
        {
            //Arrange
            var fromKey = flow.Connections.First().To;
            var toKey = flow.Connections.First().From;

            //Assert
            var preCount = flow.Connections.Count;

            //Act
            var result = flow.RemoveConnection(fromKey, toKey);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(flow);
            flow.Connections.Count.ShouldBe(preCount);
        }

        private BlockInstance CreateBlock()
        {
            var bk = A.Fake<Core.BlockKey>();
            A.CallTo(() => bk.Uuid).Returns(Guid.NewGuid());
            var block = A.Fake<Core.Block>();
            var flowBlock = A.Fake<BlockInstance>();
            A.CallTo(() => flowBlock.Block).Returns(block);
            A.CallTo(() => flowBlock.Key).Returns(bk);

            return flowBlock;
        }

        private IConnection<Core.BlockKey> CreateBlockConnection(Core.BlockKey from, Core.BlockKey to)
        {
            var bkc = A.Fake<BlockKeyConnection>();
            A.CallTo(() => bkc.From).Returns(from);
            A.CallTo(() => bkc.To).Returns(to);

            return bkc;
        }
    }
}