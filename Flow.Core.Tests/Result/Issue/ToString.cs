using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using domain = Flow.Core.Result;

namespace Flow.Core.Tests.Result.Issue
{
    [TestClass]
    public class ToString
    {
        private domain.Issue issue;

        [TestInitialize]
        public void Init()
        {
            issue = A.Fake<domain.Issue>();
            A.CallTo(() => issue.ToString()).CallsBaseMethod();
        }

        [TestMethod]
        public void WhenCalled_ACallToIssueCodeToStringMustHaveHappened()
        {
            //Act
            issue.ToString();
            
            //Assert
            A.CallTo(() => issue.IssueCode).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}