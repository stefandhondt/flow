using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using domain = Flow.Core.Result;

namespace Flow.Core.Tests.Result.Issue
{
    [TestClass]
    public class Constructor
    {
        [TestMethod]
        public void WhenCalled_PropertiesShouldMatchParameters()
        {
            //Arrange
            var code = DateTime.Now.Millisecond;
            var issueCode = new domain.IssueCode(Guid.NewGuid().ToString(), code);
            var exception = A.Fake<Exception>();
            var parameters = new[]
            {
                new domain.IssueParameter(Guid.NewGuid().ToString(), Guid.NewGuid()),
            };
            
            //Act
            var issue = new domain.Issue(issueCode, parameters, exception);
            
            //Assert
            issue.IssueCode.ShouldBe(issueCode);
            issue.Parameters.ShouldBe(parameters);
            issue.Exception.ShouldBe(exception);
        }
    }
}