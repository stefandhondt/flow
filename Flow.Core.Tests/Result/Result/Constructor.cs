using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using domain = Flow.Core.Result;

namespace Flow.Core.Tests.Result.Result
{
    [TestClass]
    public class Ok
    {
        [TestMethod]
        public void WhenCalled_ContextShouldMatchParameter()
        {
            //Arrange
            var context = Guid.NewGuid().ToString();
            
            //Act
            var result = domain.Result.Ok(context);
            
            //Assert
            result.Context.ShouldBe(context);
        }
        
        [TestMethod]
        public void WhenCalledWithoutParameter_ContextShouldMatchCallingMethodName()
        {
            //Act
            var result = domain.Result.Ok();
            
            //Assert
            result.Context.ShouldBe(nameof(WhenCalledWithoutParameter_ContextShouldMatchCallingMethodName));
        }
    }
}