﻿using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Flow.Core.Resolvers;
using Flow.Core.Result;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Resolvers.BlockResolver
{
    [TestClass]
    public class ResolveNextBlock
    {
        private IBlockResolver blockResolver;
        private List<IBlock> blocks;
        private List<IConnection<IBlock>> connections;

        [TestInitialize]
        public void Init()
        {
            blockResolver = A.Fake<Core.Resolvers.BlockResolver>();
            A.CallTo(() => blockResolver.ResolveNextBlock(A<IBlock>.Ignored, A<IEnumerable<IConnection<IBlock>>>.Ignored)).CallsBaseMethod();

            blocks = new List<IBlock>
            {
                CreateBlock("b1"),
                CreateBlock("b2"),
                CreateBlock("b3")
            };

            connections = new List<IConnection<IBlock>>
            {
                CreateConnection(blocks[0], blocks[1]),
                CreateConnection(blocks[1], blocks[2]),
            };
        }

        [TestMethod]
        public void WhenCurrentBlockIsNull_InvalidResultWithIssueCodeNoActiveBlockShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveNextBlock(null, connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.NoActiveBlock);
        }

        [TestMethod]
        public void WhenCurrentBlockDoesNotHaveAnyOutgoingConnections_InvalidResultWithIssueCodeBlockDoesNotHaveAnyOutgoingConnectionsShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveNextBlock(blocks[2], connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.BlockDoesNotHaveAnyOutgoingConnections);
        }

        [TestMethod]
        public void WhenCurrentBlockTypeIsNotSupported_InvalidResultWithIssueCodeUnsupportedBlockTypeShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.UnsupportedBlockType);
        }

        [TestMethod]
        public void WhenOnlyASingleBlockIsProvidedAndConnectionsParameterIsNull_InvalidResultWithIssueCodeBlockDoesNotHaveAnyOutgoingConnectionsShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], null);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.BlockDoesNotHaveAnyOutgoingConnections);
        }

        [TestMethod]
        public void WhenOnlyASingleBlockIsProvidedAndConnectionsParameterIsEmptyCollection_InvalidResultWithIssueCodeBlockDoesNotHaveAnyOutgoingConnectionsShouldBeReturned()
        {
            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], Enumerable.Empty<IConnection<IBlock>>());

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.BlockDoesNotHaveAnyOutgoingConnections);
        }

        #region Functional block

        [TestMethod]
        public void WhenCurrentBlockIsFunctionalBlockAndMultipleOutgoingConnectionsExist_InvalidResultWithIssueCodeFunctionalBlockCanNotHaveMultipleOutgoingConnectionsShouldBeReturned()
        {
            //Arrange
            blocks[0] = CreateFunctionalBlock("fb");
            connections[0] = CreateConnection(blocks[0], blocks[1]);
            connections.Add(CreateConnection(blocks[0], blocks[2]));

            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.FunctionalBlockCanNotHaveMultipleOutgoingConnections);
        }

        [TestMethod]
        public void WhenCurrentBlockIsFunctionalBlockAndSingleOutgoingConnectionExists_ValidResultWithBlockPointedToBySingleOutgoingConnectionShouldBeReturned()
        {
            //Arrange
            blocks[0] = CreateFunctionalBlock("fb");
            connections[0] = CreateConnection(blocks[0], blocks[1]);

            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[1]);
        }

        #endregion Functional block

        #region Conditional block

        [TestMethod]
        public void WhenCurrentBlockIsConditionalBlockAndConnectionsCollectionHasSingleConnectionWithMatchingContinuationValue_ValidResultWithBlockPointedToByMatchingConnectionShouldBeReturned()
        {
            //Arrange
            var continuationValue = 10;
            var cb = CreateConditionalBlock("cb");
            A.CallTo(() => cb.Evaluate()).Returns(continuationValue);
            blocks[0] = cb;

            connections = new List<IConnection<IBlock>>
            {
                CreateConnection(blocks[0], blocks[1], continuationValue),
                CreateConnection(blocks[0], blocks[2], 20)
            };

            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeTrue();
            result.Value.ShouldBe(blocks[1]);
        }

        [TestMethod]
        public void WhenCurrentBlockIsConditionalBlockAndConnectionsCollectionDoesNotHaveConnectionWithMatchingContinuationValue_InvalidResultWithIssueCodeNoConnectionWithMatchingContinuationValueFoundShouldBeReturned()
        {
            //Arrange
            var cb = CreateConditionalBlock("cb");
            A.CallTo(() => cb.Evaluate()).Returns(15);
            blocks[0] = cb;
            
            connections = new List<IConnection<IBlock>>
            {
                CreateConnection(blocks[0], blocks[1], 10),
                CreateConnection(blocks[0], blocks[2], 20)
            };

            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.NoConnectionWithMatchingContinuationValueFound);
        }

        [TestMethod]
        public void WhenCurrentBlockIsConditionalBlockAndConnectionsCollectionHasMultipleConnectionsWithMatchingContinuationValue_InvalidResultWithIssueCodeMultipleConnectionsWithMatchingContinuationValueFoundShouldBeReturned()
        {
            //Arrange
            var continuationValue = 10;
            var cb = CreateConditionalBlock("cb");
            A.CallTo(() => cb.Evaluate()).Returns(continuationValue);
            blocks[0] = cb;

            connections = new List<IConnection<IBlock>>
            {
                CreateConnection(blocks[0], blocks[1], continuationValue),
                CreateConnection(blocks[0], blocks[2], continuationValue)
            };

            //Act
            var result = blockResolver.ResolveNextBlock(blocks[0], connections);

            //Assert
            result.IsOk.ShouldBeFalse();
            result.Issues.ShouldContain(i => i.IssueCode == IssueCodes.BlockResolver.MultipleConnectionsWithMatchingContinuationValueFound);
            result.Issues.Single(i => i.IssueCode == IssueCodes.BlockResolver.MultipleConnectionsWithMatchingContinuationValueFound).Parameters
                .ShouldContain(p => p.Key == nameof(IConnection<object>.ContinuationValue)
                                    && (int)p.Value == continuationValue);
        }

        #endregion Conditional block

        #region Helpers

        private IBlock CreateBlock(string name)
        {
            var block = A.Fake<IBlock>();
            A.CallTo(() => block.Name).Returns(name);

            return block;
        }

        private IConditionalBlock CreateConditionalBlock(string name)
        {
            var block = A.Fake<IConditionalBlock>();
            A.CallTo(() => block.Name).Returns(name);

            return block;
        }

        private IFunctionalBlock CreateFunctionalBlock(string name)
        {
            var block = A.Fake<IFunctionalBlock>();
            A.CallTo(() => block.Name).Returns(name);

            return block;
        }

        private IConnection<IBlock> CreateConnection(IBlock from, IBlock to, object continuationValue = null)
        {
            var connection = A.Fake<Core.BlockConnection>();
            A.CallTo(() => connection.From).Returns(from);
            A.CallTo(() => connection.To).Returns(to);
            A.CallTo(() => connection.ContinuationValue).Returns(continuationValue);

            return connection;
        }

        #endregion Helpers
    }
}