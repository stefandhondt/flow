using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Flow.Core.Tests.Setting
{
    [TestClass]
    public class ToString
    {
        private Core.Setting setting;

        [TestInitialize]
        public void Init()
        {
            setting = A.Fake<Core.Setting>();
            A.CallTo(() => setting.ToString()).CallsBaseMethod();
        }
        
        [TestMethod]
        public void WhenCalled_ResultShouldContainKeyAndValue()
        {
            //Arrange
            var key = Guid.NewGuid().ToString();
            A.CallTo(() => setting.Key).Returns(key);
            var value = Guid.NewGuid().ToString();
            A.CallTo(() => setting.Value).Returns(value);
            
            //Act
            var result = setting.ToString();

            //Assert
            result.ShouldContain(key);
            result.ShouldContain(value);
        }
    }
}