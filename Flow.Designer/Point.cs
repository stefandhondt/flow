﻿namespace Flow.Designer
{
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(System.Windows.Point point)
        {
            X = (int)point.X;
            Y = (int)point.Y;
        }
    }
}