﻿using System.Windows.Input;
using MahApps.Metro.Controls;

namespace Flow.Designer.Views
{
    /// <summary>
    /// Interaction logic for DesignerView.xaml
    /// </summary>
    public partial class DesignerView
    {
        public DesignerView()
        {
            InitializeComponent();
        }
    }
}
