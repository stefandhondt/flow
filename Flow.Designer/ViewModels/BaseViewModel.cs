﻿using System.Reflection;
using Caliburn.Micro;
using Ninject;
using Ninject.Modules;

namespace Flow.Designer.ViewModels
{
    public class BaseViewModel : Screen
    {
        private static readonly StandardKernel kernel;
        protected readonly IEventAggregator EventAggregator;

        public BaseViewModel()
        {
            EventAggregator = kernel.Get<IEventAggregator>();
        }

        static BaseViewModel()
        {
            kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
        }
    }

    public class DiBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IEventAggregator>().To<EventAggregator>()
                .InSingletonScope();
        }
    }
}