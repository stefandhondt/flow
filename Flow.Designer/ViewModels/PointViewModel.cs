﻿namespace Flow.Designer.ViewModels
{
    public class PointViewModel : BaseViewModel
    {
        private int x;
        private int y;

        public int X
        {
            get => x;
            set
            {
                if(value == x) return;
                x = value;
                NotifyOfPropertyChange();
            }
        }

        public int Y
        {
            get => y;
            set
            {
                if(value == y) return;
                y = value;
                NotifyOfPropertyChange();
            }
        }

        public PointViewModel()
        {
        }

        public PointViewModel(Point point)
        {
            X = point.X;
            Y = point.Y;
        }

        public PointViewModel(System.Windows.Point point)
        {
            X = (int)point.X;
            Y = (int)point.Y;
        }
    }
}