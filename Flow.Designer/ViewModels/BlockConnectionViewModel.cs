﻿using Caliburn.Micro;
using Flow.Designer.Messages;

namespace Flow.Designer.ViewModels
{
    public class BlockConnectionViewModel : DesignerItemViewModel
    {
        public BlockViewModel From { get; set; }
        public BlockViewModel To { get; set; }

        public void Remove() => EventAggregator.PublishOnUIThread(new DesignerItemRemovedMessage<BlockConnectionViewModel>(this));
    }
}