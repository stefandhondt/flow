﻿namespace Flow.Designer.ViewModels
{
    public class DesignerItemViewModel : BaseViewModel
    {
        /// <summary>
        /// Used for moving designer items around the designer canvas.
        /// Gets or sets the difference between the top-left corner and the position where the mouse was when a drag was initiated.
        /// </summary>
        public Point DragOffset { get; set; }

        public string TypeName => GetType().Name;
    }
}