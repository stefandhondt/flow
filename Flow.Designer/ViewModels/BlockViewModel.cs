﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Flow.Core;
using Flow.Designer.Messages;
using Flow.Designer.ViewModels.Block;

namespace Flow.Designer.ViewModels
{
    public class BlockViewModel : LocationBasedDesignerItemViewModel, IHaveUniqueIdentifier
    {
        private string name;
        private string type;

        public Guid Uuid { get; set; }

        public string Name
        {
            get => name;
            set
            {
                if(value == name) return;
                name = value;
                NotifyOfPropertyChange();
            }
        }

        public string Type
        {
            get => type;
            set
            {
                if(value == type) return;
                type = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<SettingViewModel> Settings { get; }
        public BindableCollection<RequirementViewModel> Requirements { get; }
        public BindableCollection<ContributionViewModel> Contributions { get; }

        public BlockViewModel()
        {
            Settings = new BindableCollection<SettingViewModel>();
            Requirements = new BindableCollection<RequirementViewModel>();
            Contributions = new BindableCollection<ContributionViewModel>();
        }

        public BlockViewModel(IBlock block)
            : this()
        {
            if(block == null)
                return;

            Name = block.Name;
            Type = block.Type;

            Settings.AddRange(block.Settings?.Select(s => new SettingViewModel(s)));
            Requirements.AddRange(block.Requirements?.Select(r => new RequirementViewModel(r)));
            Contributions.AddRange(block.Contributions?.Select(c => new ContributionViewModel(c)));
        }

        public BlockViewModel(IBlock block, Guid uuid, Point location)
            : this(block)
        {
            if(block == null)
                return;

            Uuid = uuid;
            Location = new PointViewModel(location);
        }

        public BlockViewModel(BlockViewModel block)
            : this()
        {
            Uuid = block.Uuid;
            Name = block.Name;
            Type = block.Type;
            Location = block.Location;

            foreach(var setting in block.Settings)
                Settings.Add(new SettingViewModel(setting));

            foreach(var requirement in block.Requirements)
                Requirements.Add(new RequirementViewModel(requirement));

            foreach(var contribution in block.Contributions)
                Contributions.Add(new ContributionViewModel(contribution));
        }

        public void Dragging(object sender, MouseEventArgs e)
        {
            if(!(sender is DependencyObject dependencyObject))
                return;

            if(e.LeftButton != MouseButtonState.Pressed)
                return;

            var data = new DataObject(GetType().Name, this);
            DragDrop.DoDragDrop(dependencyObject, data, DragDropEffects.Copy);
        }

        public void Moving(object sender, MouseEventArgs e)
        {
            if(!(sender is DependencyObject dependencyObject))
                return;

            if(e.LeftButton != MouseButtonState.Pressed)
                return;

            var offset = Mouse.GetPosition((IInputElement)sender);
            DragOffset = new Point(offset);
            var data = new DataObject(GetType().Name, this);
            DragDrop.DoDragDrop(dependencyObject, data, DragDropEffects.Move);
        }

        public void Remove() => EventAggregator.PublishOnUIThread(new DesignerItemRemovedMessage<BlockViewModel>(this));
    }
}