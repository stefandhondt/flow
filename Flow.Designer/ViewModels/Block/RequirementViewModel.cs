﻿using System;
using Caliburn.Micro;
using Flow.Core;

namespace Flow.Designer.ViewModels.Block
{
    public class RequirementViewModel : Screen
    {
        private string property;
        private Type type;
        private object value;

        public string Property
        {
            get => property;
            set
            {
                if(value == property) return;
                property = value;
                NotifyOfPropertyChange();
            }
        }

        public Type Type
        {
            get => type;
            set
            {
                if(Equals(value, type)) return;
                type = value;
                NotifyOfPropertyChange();
            }
        }

        public object Value
        {
            get => value;
            set
            {
                if(Equals(value, this.value)) return;
                this.value = value;
                NotifyOfPropertyChange();
            }
        }

        public RequirementViewModel()
        {
        }

        public RequirementViewModel(IRequirement requirement)
            : this()
        {
            if(requirement == null)
                return;

            Property = requirement.Property.Name;
            Value = requirement.Value;
            Type = requirement.Property.PropertyType;
        }

        public RequirementViewModel(RequirementViewModel requirement)
            : this()
        {
            Property = requirement.Property;
            Type = requirement.Type;
            Value = requirement.Value;
        }
    }
}