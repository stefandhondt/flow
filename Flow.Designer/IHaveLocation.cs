﻿using Flow.Designer.ViewModels;

namespace Flow.Designer
{
    public interface IHaveLocation
    {
        PointViewModel Location { get; set; }
    }
}