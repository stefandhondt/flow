﻿using System;
using System.Globalization;
using System.IO;
using Flow.Wpf.Shared.Converters;

namespace Flow.Designer.Converters
{
    public class BlockTypeToImagePathConverter : ValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(value is string strValue))
                return null;

            var path = $"../../Resources/Images/{strValue}.png";

            return Path.GetFullPath(path);
        }
    }
}