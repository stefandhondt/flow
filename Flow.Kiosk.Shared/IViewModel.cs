using Flow.Core;

namespace Flow.Kiosk.Shared
{
    public interface IViewModel<out T> where T: IBlock
    {
        T Model { get; }

        void SetModel(IBlock model);
    }
}