﻿using System.Windows;

namespace Flow.Kiosk.Shared.AttachedProperties
{
    public class Keyboard
    {
        public static readonly DependencyProperty KeyboardConfigurationSettingProperty =
            DependencyProperty.RegisterAttached("KeyboardConfigurationSetting",
                typeof(string), typeof(Keyboard),
                new PropertyMetadata(null));

        public static void SetKeyboardConfigurationSetting(DependencyObject element, string value)
        {
            element.SetValue(KeyboardConfigurationSettingProperty, value);
        }

        public static string GetKeyboardConfigurationSetting(DependencyObject element)
        {
            return (string)element.GetValue(KeyboardConfigurationSettingProperty);
        }

        public static readonly DependencyProperty KeyboardConfigurationProperty =
            DependencyProperty.RegisterAttached("KeyboardConfiguration",
                typeof(string), typeof(Keyboard),
                new PropertyMetadata(null));

        public static void SetKeyboardConfiguration(DependencyObject element, string value)
        {
            element.SetValue(KeyboardConfigurationProperty, value);
        }

        public static string GetKeyboardConfiguration(DependencyObject element)
        {
            return (string)element.GetValue(KeyboardConfigurationProperty);
        }
    }
}