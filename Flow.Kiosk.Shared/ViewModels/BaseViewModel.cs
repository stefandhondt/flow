﻿using Agidens.Logging;
using Caliburn.Micro;
using Flow.Core;

namespace Flow.Kiosk.Shared.ViewModels
{
    public abstract class BaseViewModel<T> : Screen, IViewModel<T> where T:IBlock
    {
        public ILogger Logger { get; }
        public ILoggerFactory LoggerFactory { get; }
        public T Model { get; private set; }
        
        public BaseViewModel(ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
            Logger = LoggerFactory?.GetLogger(this);
        }

        public void SetModel(IBlock model)
        {
            if(model is T typedModel)
            {
                Model = typedModel;
            }
        }
    }
}