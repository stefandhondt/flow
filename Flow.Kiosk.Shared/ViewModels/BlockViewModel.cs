﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using Caliburn.Micro;
//using Flow.Core;
//using Flow.Core.Attributes;
//using Flow.Core.Result;
//
//namespace Flow.Kiosk.Shared.ViewModels
//{
//    public abstract class BlockViewModel : Screen, IBlock
//    {
//        private List<ISetting> settings = null;
//        private List<IContribution> contributions = null;
//        private List<IRequirement> requirements = null;
//
//        public BlockViewModel(string name, string description = null)
//        {
//            Name = name;
//            Description = description;
//        }
//
//        #region IBlock
//
//        public virtual string Name { get; }
//        public virtual string Description { get; }
//        public virtual string Type => GetType().FullName;
//
//        public event EventHandler Validated;
//        public event EventHandler Activated;
//        public event EventHandler Deactivated;
//
//        public virtual IValidationResult Validate()
//        {
//            OnValidated(new BlockEventArgs());
//            return ValidationResult.Ok();
//        }
//
//        public virtual IActivationResult Activate()
//        {
//            OnActivated(new BlockEventArgs());
//            return ActivationResult.Ok();
//        }
//
//        public virtual IActivationResult Deactivate()
//        {
//            OnDeactivated(new BlockEventArgs());
//            return ActivationResult.Ok();
//        }
//
//        public virtual IReadOnlyCollection<IRequirement> Requirements
//        {
//            get
//            {
//                if(requirements == null)
//                {
//                    requirements = new List<IRequirement>();
//                    var propsWithRequirement = GetProperties().Where(p => Attribute.IsDefined((MemberInfo)p, typeof(RequirementAttribute))).ToList();
//                    requirements.AddRange(propsWithRequirement.Select(p => new Requirement
//                    {
//                        Property = p,
//                        Value = p.GetValue(this)
//                    }));
//                }
//                else //Update requirement values.
//                {
//                    foreach(var requirement in requirements)
//                        requirement.Value = requirement.Property.GetValue(this);
//                }
//
//                return requirements;
//            }
//        }
//
//        public virtual IReadOnlyCollection<IContribution> Contributions
//        {
//            get
//            {
//                if(contributions == null)
//                {
//                    contributions = new List<IContribution>();
//                    var propsWithContribution = GetProperties().Where(p => Attribute.IsDefined(p, typeof(ContributionAttribute))).ToList();
//                    contributions.AddRange(propsWithContribution.Select(p => new Contribution
//                    {
//                        Property = p,
//                        Value = p.GetValue(this)
//                    }));
//                }
//                else //Update contribution values.
//                {
//                    foreach(var contribution in contributions)
//                        contribution.Value = contribution.Property.GetValue(this);
//                }
//
//                return contributions;
//            }
//        }
//
//        public virtual IReadOnlyCollection<ISetting> Settings
//        {
//            get
//            {
//                if(settings == null)
//                {
//                    settings = new List<ISetting>();
//                    var attributes = GetSettingAttributes();
//
//                    foreach(var attribute in attributes)
//                        settings.Add(new Setting(attribute.Name, attribute.DefaultValue));
//                }
//
//                return settings.AsReadOnly();
//            }
//        }
//
//        public IBlock ResetSettingValue(string name)
//        {
//            var attributes = GetSettingAttributes();
//
//            return SetSettingValue(name, attributes.FirstOrDefault(a => a.Name == name)?.DefaultValue);
//        }
//
//        public IBlock SetSettingValue(string name, string value)
//        {
//            var setting = settings.FirstOrDefault(s => s.Key == name);
//
//            if(setting != null)
//                setting.Value = value;
//
//            return this;
//        }
//
//        #endregion IBlock
//
//        public override string ToString()
//        {
//            var sb = new StringBuilder();
//            var type = GetType();
//
//            sb.AppendLine(string.Concat(Enumerable.Repeat("*", 35)));
//            sb.AppendLine($"{type.FullName} - {Name}");
//
//            if(!string.IsNullOrWhiteSpace(Description))
//                sb.AppendLine(Description);
//
//            sb.AppendLine($"  {string.Concat(Enumerable.Repeat("-", 25))}");
//            sb.AppendLine($"  Requirements ({Requirements.Count})");
//
//            foreach(var requirement in Requirements)
//                sb.AppendLine($"    {requirement}");
//
//            sb.AppendLine($"  {string.Concat(Enumerable.Repeat("-", 25))}");
//
//            sb.AppendLine($"  Contributions ({Contributions.Count})");
//
//            foreach(var contribution in Contributions)
//                sb.AppendLine($"    {contribution}");
//
//            sb.AppendLine($"  {string.Concat(Enumerable.Repeat("-", 25))}");
//
//            sb.AppendLine($"  Settings ({Settings.Count})");
//
//            foreach(var setting in Settings)
//                sb.AppendLine($"    {setting}");
//
//            sb.AppendLine($"  {string.Concat(Enumerable.Repeat("-", 25))}");
//            sb.AppendLine(string.Concat(Enumerable.Repeat("*", 35)));
//
//            return sb.ToString();
//        }
//
//        public virtual object Clone() => MemberwiseClone();
//
//        #region Helpers
//
//        private IEnumerable<PropertyInfo> GetProperties()
//        {
//            var type = GetType();
//            var props = type.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
//
//            return props;
//        }
//
//        private IEnumerable<SettingAttribute> GetSettingAttributes()
//        {
//            var baseSettingsAttributes = GetType().BaseType?.GetCustomAttributes(typeof(SettingAttribute), true)?.Cast<SettingAttribute>()?.ToList()
//                                         ?? new List<SettingAttribute>();
//            var currentBlockSettingsAttributes = GetType().GetCustomAttributes(typeof(SettingAttribute), false).Cast<SettingAttribute>().ToList();
//            //Remove all settings from possible base type which are overridden in current blockInstance.
//            baseSettingsAttributes.RemoveAll(a => currentBlockSettingsAttributes.Any(sa => sa.Name == a.Name));
//
//            var attributes = baseSettingsAttributes.Concat(currentBlockSettingsAttributes);
//
//            return attributes;
//        }
//
//        private void OnValidated(BlockEventArgs args)
//        {
//            Validated?.Invoke(this, args);
//        }
//
//        private void OnActivated(BlockEventArgs args)
//        {
//            Activated?.Invoke(this, args);
//        }
//
//        private void OnDeactivated(BlockEventArgs args)
//        {
//            Deactivated?.Invoke(this, args);
//        }
//
//        #endregion Helpers
//    }
//}