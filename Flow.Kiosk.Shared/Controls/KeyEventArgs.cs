﻿using System;

namespace Flow.Kiosk.Shared.Controls
{
    public class KeyEventArgs : EventArgs
    {
        public OnScreenKey Key { get; set; }
    }
}