using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Kiosk.Blocks.DisplayMessage
{
    [Setting("MessageResourceKey", description: "Resource key to retrieve and display on screen in the active culture.")]
    public class DisplayMessageModel : FunctionalBlock
    {
        [Requirement]
        public string Culture { get; set; }
        public string ResourceKey => Settings.SingleOrDefault(s => s.Key == "MessageResourceKey")?.Value;
        
        public DisplayMessageModel(ILoggerFactory loggerFactory)
            : base("Display Message", null, loggerFactory)
        {
        }
    }
}