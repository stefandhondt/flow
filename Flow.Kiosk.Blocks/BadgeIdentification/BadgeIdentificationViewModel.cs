﻿using Agidens.Logging;
using Flow.Kiosk.Shared.Attributes;
using Flow.Kiosk.Shared.ViewModels;

namespace Flow.Kiosk.Blocks.BadgeIdentification
{
    [Priority(0)]
    public class BadgeIdentificationViewModel : BaseViewModel<BadgeIdentificationModel>
    {
        private string identifier;

        /// <summary>
        /// Gets or sets the identifier of the identification.
        /// </summary>
        public string Identifier
        {
            get => identifier;
            set
            {
                if(value == identifier) return;
                identifier = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange(nameof(CanAccept));
            }
        }
        
        public BadgeIdentificationViewModel(ILoggerFactory loggerFactory, BadgeIdentificationModel model)
            : base(loggerFactory)
        {
            SetModel(model);
        }

        /// <summary>
        /// Determines if an identification can be requested from the backend.
        /// </summary>
        /// <value>
        ///   <c>true</c> if an identification can be requested from the backend; <c>false</c> otherwise.
        /// </value>
        public bool CanAccept => !string.IsNullOrWhiteSpace(Identifier);

        /// <summary>
        /// Requests an identification from the backend for the current <c>Identifier</c>.
        /// </summary>
        public void Accept()
        {
            if(CanAccept)
            {
                Logger?.Log(LogLevels.Info, $"Retrieving badge using identifier '{Identifier}'.");
                Model.SetIdentification(Identifier);

                Logger?.Log(LogLevels.Info, Model.Identification == null 
                    ? $"Failed to retrieve badge using identifier '{Identifier}'."
                    : $"Retrieved badge with key '{Model.Identification.Key}' using identifier '{Identifier}'.");

                NotifyOfPropertyChange(nameof(CanNext));
            }
        }

        /// <summary>
        /// Determines whether we can advance the flow to the next block.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the flow can be advanced to the next block; <c>false</c> otherwise.
        /// </returns>
        public bool CanNext => Model?.Identification != null;

        /// <summary>
        /// Advances the flow to the next block.
        /// </summary>
        public void Next()
        {
            if(CanNext)
            {
                Model?.Deactivate();
            }
        }
    }
}