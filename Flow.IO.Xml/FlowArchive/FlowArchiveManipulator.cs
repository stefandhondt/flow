﻿namespace Flow.IO.Xml.FlowArchive
{
    public class FlowArchiveManipulator
    {
        protected readonly string FlowArchiveExtension = ".flow";
        protected readonly string FlowFileExtension = ".xml";
        protected readonly string DesignerFileExtension = ".designer.xml";
    }
}