﻿using System.Xml.Linq;

namespace Flow.IO.Xml.FlowArchive
{
    public interface IFlowArchive
    {
        string Name { get; set; }
        XDocument FlowFile { get; }
        XDocument DesignerFile { get; }
    }
}