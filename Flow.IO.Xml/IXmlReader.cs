﻿using System.Xml.Linq;
using Flow.Core;
using Flow.Core.Result;

namespace Flow.IO.Xml
{
    public interface IXmlReader
    {
        IResult<IFlow> Load(string uri);
        IResult<IFlow> Load(XDocument xml);
    }
}