﻿using System.Collections.Generic;
using System.Reflection;

namespace Flow.IO.Resolvers
{
    public interface IAssemblyResolver
    {
        IEnumerable<Assembly> Resolve(string path);
    }
}