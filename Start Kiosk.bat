@echo off
:start
echo start Kiosk
set Source="C:\Projects\Flow"
set Destination="C:\Projects\Flow\_Local"

::clear destination folder
cd %Destination%
del * /f /q
cd %Source%
xcopy ".\Flow.Kiosk.Shell\bin\Debug" "%Destination%" /s /f /d /e /r /y /k
xcopy ".\Flow.Ninject\bin\Debug\*Ninject.*" "%Destination%" /s /f /d /e /r /y /k
xcopy ".\Flow.Kiosk.Blocks\bin\Debug\*.*" "%Destination%" /s /f /d /e /r /y /k

cd %Destination%
start Flow.Kiosk.Shell.exe -n -f "C:\Projects\Flow\Flow.Console\Resources\Flow.v2c.flow"