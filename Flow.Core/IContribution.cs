﻿using System.Reflection;

namespace Flow.Core
{
    public interface IContribution : IHaveAlias
    {
        PropertyInfo Property { get; set; }
        object Value { get; set; }
    }
}