﻿using System;

namespace Flow.Core
{
    public class BlockKey
    {
        public virtual Guid Uuid { get; }

        public BlockKey(Guid uuid)
        {
            Uuid = uuid;
        }

        public override bool Equals(object obj) => obj is BlockKey bk && bk.Uuid == Uuid;
        public override int GetHashCode() => Uuid.GetHashCode();

        public static bool operator ==(BlockKey lhs, BlockKey rhs) => Equals(lhs, rhs);
        public static bool operator !=(BlockKey lhs, BlockKey rhs) => !Equals(lhs, rhs);

        public override string ToString()
        {
            return $"{nameof(Uuid)}: {Uuid}";
        }
    }
}