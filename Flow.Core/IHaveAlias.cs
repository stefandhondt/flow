﻿namespace Flow.Core
{
    public interface IHaveAlias
    {
        string Alias { get; set; }
    }
}