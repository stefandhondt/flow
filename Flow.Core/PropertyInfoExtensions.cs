﻿using System;
using System.Reflection;

namespace Flow.Core
{
    public static class PropertyInfoExtensions
    {
        public static T GetAttribute<T>(this PropertyInfo prop) where T : Attribute
        {
            if(prop == null)
                return default(T);

            if(!Attribute.IsDefined(prop, typeof(T)))
                return default(T);

            var attr = (T)Attribute.GetCustomAttribute(prop, typeof(T));

            return attr;
        }
    }
}