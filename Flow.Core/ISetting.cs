﻿using System.Runtime.CompilerServices;

namespace Flow.Core
{
    public interface ISetting
    {
        string Key { get; set; }
        string DefaultValue { get; set; }
        string Description { get; set; }
        string Value { get; set; }
    }
}