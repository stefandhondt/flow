﻿using System.Collections.Generic;

namespace Flow.Core
{
    public interface IConnection<out T>
    {
        T From { get; }
        T To { get; }
        object ContinuationValue { get; }
    }
}