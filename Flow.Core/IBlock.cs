﻿using System;
using System.Collections.Generic;
using Flow.Core.Result;

namespace Flow.Core
{
    public interface IBlock : ICloneable
    {
        string Name { get; }
        string Description { get; }
        string Type { get; }

        IReadOnlyCollection<IRequirement> Requirements { get; }
        IReadOnlyCollection<IContribution> Contributions { get; }
        IReadOnlyCollection<ISetting> Settings { get; }

        IBlock ResetSettingValue(string name);
        IBlock SetSettingValue(string name, string value);

        IResult<IBlock> Validate();
        IResult<IBlock> Activate();
        IResult<IBlock> Deactivate();

        event EventHandler Validated;
        event EventHandler Activated;
        event EventHandler Deactivated;
    }
}