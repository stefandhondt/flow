﻿namespace Flow.Core
{
    public class BlockInstance
    {
        public virtual BlockKey Key { get; set; }
        public virtual IBlock Block { get; set; }
    }
}