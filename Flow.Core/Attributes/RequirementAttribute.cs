﻿using System;

namespace Flow.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class RequirementAttribute : Attribute
    {
    }
}