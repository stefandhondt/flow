﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Core.Resolvers
{
    public class MostRecentFirstRequirementResolver : IRequirementResolver
    {
        public virtual IResult<IRequirement> IsRequirementMet(IRequirement requirement, IEnumerable<IEnumerable<IContribution>> allContributions)
        {
            var result = CompositeResult<IRequirement>.Ok();

            if(requirement == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(requirement))
                });
            }

            if(result.IsOk)
            {
                if(requirement.Property == null)
                {
                    result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                    {
                        IssueParameters.General.Argument.WithValue(nameof(requirement.Property))
                    });
                }
            }

            if(allContributions == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(allContributions))
                });
            }

            if(result.IsOk)
            {
                var reqAttr = requirement.Property.GetCustomAttribute<RequirementAttribute>();

                if(reqAttr == null)
                {
                    result.AddIssue(IssueCodes.RequirementResolver.RequirementAttributeNotFound, new[]
                    {
                        IssueParameters.RequirementResolver.PropertyName.WithValue(requirement.Property.Name)
                    });
                }

                if(result.IsOk)
                {
                    var assignable = false;

                    foreach(var block in allContributions)
                    {
                        if(block == null
                           || !block.Any())
                        {
                            continue;
                        }

                        foreach(var contribution in block)
                        {
                            if(contribution.Alias != requirement.Alias)
                            {
                                continue;
                            }

                            var reqType = requirement.Property.PropertyType;
                            var contrType = contribution.Property?.PropertyType;

                            if(reqType.IsAssignableFrom(contrType))
                            {
                                assignable = true;
                            }
                        }
                    }

                    if(!assignable)
                    {
                        result.AddIssue(IssueCodes.RequirementResolver.NoMatchingContributionsFound);
                    }
                }
            }

            return result;
        }

        public virtual IResult<IRequirement> SatisfyRequirement(IRequirement requirement, IBlock block, IEnumerable<IEnumerable<IContribution>> accumulatedContributions)
        {
            var result = CompositeResult<IRequirement>.Ok();

            if(requirement == null)
            {
                result.AddIssue(IssueCodes.General.ArgumentNull, new[]
                {
                    IssueParameters.General.Argument.WithValue(nameof(requirement))
                });
            }

            if(result.IsOk)
            {
                result.AddResult(IsRequirementMet(requirement, accumulatedContributions));

                if(result.IsOk)
                {
                    var reqAttr = requirement.Property?.GetCustomAttribute<RequirementAttribute>();

                    if(reqAttr == null)
                    {
                        result.AddIssue(IssueCodes.RequirementResolver.RequirementAttributeNotFound, new[]
                        {
                            IssueParameters.RequirementResolver.PropertyName.WithValue(requirement.Property.Name)
                        });
                    }

                    if(result.IsOk)
                    {
                        //Prefer the most recent contribution if the same contribution is made by multiple blocks.
                        accumulatedContributions = accumulatedContributions.Reverse();

                        foreach(var previousBlock in accumulatedContributions)
                        {
                            if(previousBlock == null
                               || !previousBlock.Any())
                            {
                                continue;
                            }

                            foreach(var contribution in previousBlock)
                            {
                                if(contribution?.Property == null)
                                    continue;

                                if(contribution.Alias != requirement.Alias)
                                    continue;

                                var reqType = requirement.Property.PropertyType;
                                var contrType = contribution.Property.PropertyType;

                                if(!reqType.IsAssignableFrom(contrType))
                                    continue;

                                requirement.Property.SetValue(block, contribution.Value);
                                requirement.Value = contribution.Value;
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}