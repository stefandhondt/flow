﻿using System.Collections.Generic;
using System.Linq;
using Flow.Core.Result;

namespace Flow.Core.Resolvers
{
    public class BlockResolver : IBlockResolver
    {
        public virtual IResult<IBlock> ResolveNextBlock(IBlock currentBlock, IEnumerable<IConnection<IBlock>> connections)
        {
            var actionResult = Result<IBlock>.Ok();

            if(connections == null)
            {
                connections = Enumerable.Empty<IConnection<IBlock>>();
            }

            if(currentBlock == null)
            {
                actionResult.AddIssue(IssueCodes.BlockResolver.NoActiveBlock);
            }
            else
            {
                var connectionsFromCurrentBlock = connections
                    .Where(c => c.From == currentBlock)
                    .ToList();

                if(!connectionsFromCurrentBlock.Any())
                {
                    actionResult.AddIssue(IssueCodes.BlockResolver.BlockDoesNotHaveAnyOutgoingConnections);
                }
                else
                {
                    if(currentBlock is IConditionalBlock conditionalBlock)
                    {
                        var result = conditionalBlock.Evaluate();
                        var resolvedBlocks = connectionsFromCurrentBlock
                            .Where(c => c.ContinuationValue.Equals(result))
                            .ToList();

                        if(resolvedBlocks.Count == 1)
                        {
                            actionResult.Value = resolvedBlocks.Single().To;
                        }
                        else if(resolvedBlocks.Count == 0)
                        {
                            actionResult.AddIssue(IssueCodes.BlockResolver.NoConnectionWithMatchingContinuationValueFound);
                        }
                        else
                        {
                            actionResult.AddIssue(IssueCodes.BlockResolver.MultipleConnectionsWithMatchingContinuationValueFound, new []{ new IssueParameter(nameof(IConnection<object>.ContinuationValue), result) });
                        }
                    }
                    else if(currentBlock is IFunctionalBlock functionalBlock)
                    {
                        if(connectionsFromCurrentBlock.Count > 1)
                        {
                            actionResult.AddIssue(IssueCodes.BlockResolver.FunctionalBlockCanNotHaveMultipleOutgoingConnections);
                        }
                        else
                        {
                            actionResult.Value = connectionsFromCurrentBlock.Single().To;
                        }
                    }
                    else
                    {
                        actionResult.AddIssue(IssueCodes.BlockResolver.UnsupportedBlockType);
                    }
                }
            }

            return actionResult;
        }

        public virtual IResult<IBlock> ResolveInitialBlock(IEnumerable<IBlock> blocks, IEnumerable<IConnection<IBlock>> connections)
        {
            var actionResult = Result<IBlock>.Ok();

            if(connections == null)
            {
                connections = Enumerable.Empty<IConnection<IBlock>>();
            }

            if(!(blocks?.Any() ?? false))
            {
                actionResult.AddIssue(IssueCodes.BlockResolver.BlockCollectionIsEmpty);
            }
            else
            {
                if(blocks.Count() == 1)
                {
                    actionResult.Value = blocks.SingleOrDefault();
                }

                var blocksWithoutIncomingConnections = new List<IBlock>();

                foreach(var block in blocks)
                {
                    var bs = connections.Select(c => c.To)
                        .Distinct()
                        .Contains(block);

                    if(!bs)
                    {
                        blocksWithoutIncomingConnections.Add(block);
                    }
                }

                if(blocksWithoutIncomingConnections.Count == 1)
                {
                    actionResult.Value = blocksWithoutIncomingConnections.SingleOrDefault();
                }
                else
                {
                    actionResult.AddIssue(IssueCodes.BlockResolver.InitialBlockCouldNotBeDetermined);
                }
            }

            return actionResult;
        }
    }
}