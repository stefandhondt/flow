﻿using System.Collections.Generic;
using Flow.Core.Result;

namespace Flow.Core.Resolvers
{
    public interface IBlockResolver
    {
        IResult<IBlock> ResolveNextBlock(IBlock currentBlock, IEnumerable<IConnection<IBlock>> connections);
        IResult<IBlock> ResolveInitialBlock(IEnumerable<IBlock> blocks, IEnumerable<IConnection<IBlock>> connections);
    }
}