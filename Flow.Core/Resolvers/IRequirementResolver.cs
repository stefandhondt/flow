﻿using System.Collections.Generic;
using Flow.Core.Result;

namespace Flow.Core.Resolvers
{
    public interface IRequirementResolver
    {
        IResult<IRequirement> IsRequirementMet(IRequirement requirement, IEnumerable<IEnumerable<IContribution>> allContributions);
        IResult<IRequirement> SatisfyRequirement(IRequirement requirement, IBlock block, IEnumerable<IEnumerable<IContribution>> accumulatedContributions);
    }
}