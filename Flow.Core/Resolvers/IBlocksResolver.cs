﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Flow.Core.Resolvers
{
    public interface IBlocksResolver
    {
        IEnumerable<Type> Resolve(IEnumerable<Assembly> assemblies);
    }
}