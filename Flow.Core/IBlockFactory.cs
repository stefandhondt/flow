﻿using System.Collections.Generic;
using Flow.Core.Result;

namespace Flow.Core
{
    public interface IBlockFactory
    {
        IEnumerable<string> GetAvailableBlockTypeNames();
        IResult<IBlock> CreateBlock(string name);
    }
}