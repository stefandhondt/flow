﻿using System.Collections.Generic;
using Agidens.Logging;

namespace Flow.Core
{
    public abstract class BoolCondition : Block, IConditionalBlock
    {
        public BoolCondition(string name, string description, ILoggerFactory loggerFactory)
            : base(name, description, loggerFactory)
        {
            ConnectionPoints = new HashSet<object>
            {
                true,
                false
            };
        }

        public abstract object Evaluate();
        public HashSet<object> ConnectionPoints { get; }
    }
}