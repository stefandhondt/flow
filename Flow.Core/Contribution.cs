﻿using System.Reflection;

namespace Flow.Core
{
    public class Contribution : IContribution
    {
        private string alias;

        public virtual PropertyInfo Property { get; set; }
        public virtual object Value { get; set; }

        public virtual string Alias
        {
            get => !string.IsNullOrWhiteSpace(alias) ? alias : Property?.Name;
            set => alias = value;
        }

        public override string ToString()
        {
            return $"{GetType().FullName} - {Property?.Name}<{Property?.PropertyType}>({Alias}) - {Value ?? "NULL"}";
        }
    }
}