﻿using Agidens.Logging;

namespace Flow.Core
{
    public abstract class FunctionalBlock : Block, IFunctionalBlock
    {
        public FunctionalBlock(string name, string description, ILoggerFactory loggerFactory)
            : base(name, description, loggerFactory)
        {
        }
    }
}