﻿using System.Collections.Generic;
using Flow.Core.Result;

namespace Flow.Core
{
    public interface IFlow
    {
        List<BlockInstance> Blocks { get; }
        List<IConnection<BlockKey>> Connections { get; }

        IResult<IFlow> Validate();
        IResult<BlockInstance> GetInitialBlock();

        IResult<IFlow> AddBlock(IBlock block);
        IResult<IFlow> AddBlock(BlockKey key, IBlock block);
        IResult<IFlow> RemoveBlock(BlockKey key);
        IResult<IFlow> AddConnection(BlockKey from, BlockKey to, object continuationValue = null);
        IResult<IFlow> RemoveConnection(BlockKey from, BlockKey to);
    }
}