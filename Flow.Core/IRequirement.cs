﻿using System.Reflection;

namespace Flow.Core
{
    public interface IRequirement : IHaveAlias
    {
        PropertyInfo Property { get; set; }
        object Value { get; set; }
    }
}