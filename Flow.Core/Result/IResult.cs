﻿using System;
using System.Collections.Generic;

namespace Flow.Core.Result
{
    public interface IResult
    {
        string Context { get; }
        bool IsOk { get; }
        IReadOnlyList<Issue> Issues { get; }

        IResult AddIssue(IssueCode issueCode, IEnumerable<IssueParameter> parameters = null, Exception exception = null);
        IResult AddIssue(Issue issue);
    }

    public interface IResult<T> : IResult
    {
        T Value { get; set; }
    }
}