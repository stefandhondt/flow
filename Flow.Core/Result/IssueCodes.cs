﻿using System.Diagnostics.CodeAnalysis;

namespace Flow.Core.Result
{
    [ExcludeFromCodeCoverage]
    public static class IssueCodes
    {
        public static class General
        {
            public const string Prefix = "CGEN";

            public static readonly IssueCode Unexpected = new IssueCode(Prefix, 1);
            public static readonly IssueCode ArgumentNull = new IssueCode(Prefix, 2);
            public static readonly IssueCode EmptyString = new IssueCode(Prefix, 3);
        }

        public static class BlockFactory
        {
            public const string Prefix = "CBLF";

            public static readonly IssueCode BlockTypeDoesntExist = new IssueCode(Prefix, 1);
            public static readonly IssueCode BlockCreationUnsuccessful = new IssueCode(Prefix, 2);
        }

        public static class BlockResolver
        {
            public const string Prefix = "CBLR";

            public static readonly IssueCode NoActiveBlock = new IssueCode(Prefix, 1);
            public static readonly IssueCode BlockDoesNotHaveAnyOutgoingConnections = new IssueCode(Prefix, 2);
            public static readonly IssueCode NoConnectionWithMatchingContinuationValueFound = new IssueCode(Prefix, 3);
            public static readonly IssueCode MultipleConnectionsWithMatchingContinuationValueFound = new IssueCode(Prefix, 4);
            public static readonly IssueCode FunctionalBlockCanNotHaveMultipleOutgoingConnections = new IssueCode(Prefix, 5);
            public static readonly IssueCode UnsupportedBlockType = new IssueCode(Prefix, 6);
            public static readonly IssueCode BlockCollectionIsEmpty = new IssueCode(Prefix, 7);
            public static readonly IssueCode InitialBlockCouldNotBeDetermined = new IssueCode(Prefix, 8);
        }

        public static class Flow
        {
            public const string Prefix = "CFLW";

            public static readonly IssueCode BlockCollectionEmpty = new IssueCode(Prefix, 1);
            public static readonly IssueCode BlockIdentifierNotUnique = new IssueCode(Prefix, 2);
            public static readonly IssueCode FlowHasNoConnections = new IssueCode(Prefix, 3);
            public static readonly IssueCode BlockNotFoundInCollection = new IssueCode(Prefix, 4);
            public static readonly IssueCode DuplicateConnection = new IssueCode(Prefix, 5);
        }

        public static class FlowArchiveReader
        {
            public const string Prefix = "CFAR";

            public static readonly IssueCode IncorrectExtension = new IssueCode(Prefix, 1);
            public static readonly IssueCode ArchiveDoesNotContaineFlowFile = new IssueCode(Prefix, 2);
            public static readonly IssueCode ArchiveDoesNotContaineDesignerFile = new IssueCode(Prefix, 3);
        }

        public static class FlowArchiveWriter
        {
            public const string Prefix = "CFAW";
        }
        
        public static class FlowRunner
        {
            public const string Prefix = "CFLR";

            public static readonly IssueCode FlowNotDefined = new IssueCode(Prefix, 1);
            public static readonly IssueCode FlowActive = new IssueCode(Prefix, 2);
            public static readonly IssueCode FlowNotActive = new IssueCode(Prefix, 3);
            public static readonly IssueCode ActiveBlockHasNoOutgoingConnections = new IssueCode(Prefix, 4);
        }

        public static class RequirementResolver
        {
            public const string Prefix = "CRQR";

            public static readonly IssueCode RequirementAttributeNotFound = new IssueCode(Prefix, 1);
            public static readonly IssueCode NoMatchingContributionsFound = new IssueCode(Prefix, 2);
        }

        public static class XmlReader
        {
            public const string Prefix = "CXMR";

            public static readonly IssueCode IncorrectExtension = new IssueCode(Prefix, 1);
            public static readonly IssueCode FileDoesNotExist = new IssueCode(Prefix, 2);
        }
    }
}