﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Flow.Core.Result
{
    [Serializable]
    public class Result : IResult
    {
        protected readonly List<Issue> issues = new List<Issue>();

        public static Result Ok([CallerMemberName] string context = null) => new Result(context);

        protected Result([CallerMemberName] string context = null)
        {
            Context = context;
        }

        public static implicit operator bool(Result result) => !result.Issues.Any();

        #region IResult

        public virtual string Context { get; }
        public virtual bool IsOk => this;
        public virtual IReadOnlyList<Issue> Issues => issues;

        public virtual IResult AddIssue(IssueCode issueCode, IEnumerable<IssueParameter> parameters = null, Exception exception = null)
        {
            AddIssue(new Issue(issueCode, parameters, exception));

            return this;
        }

        public virtual IResult AddIssue(Issue issue)
        {
            if(issue != null)
            {
                issues.Add(issue);
            }

            return this;
        }

        #endregion IResult
    }

    public class Result<T> : Result, IResult<T>
    {
        public virtual T Value { get; set; }

        public Result(T value = default(T), [CallerMemberName] string context = null)
            : base(context)
        {
            Value = value;
        }

        public static Result<T> Ok([CallerMemberName] string context = null) => new Result<T>(default(T), context);
        public static Result<T> Ok(T value, [CallerMemberName] string context = null) => new Result<T>(value, context);

        public new virtual Result<T> AddIssue(IssueCode issueCode, IEnumerable<IssueParameter> parameters = null, Exception exception = null)
        {
            AddIssue(new Issue(issueCode, parameters, exception));

            return this;
        }

        public new virtual Result<T> AddIssue(Issue issue)
        {
            if(issue != null)
            {
                issues.Add(issue);
            }

            return this;
        }
    }
}