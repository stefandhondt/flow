﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Flow.Core.Result
{
    [Serializable]
    public class CompositeResult : Result
    {
        private readonly List<IResult> results;

        public virtual IReadOnlyList<IResult> Results => results;

        public new static CompositeResult Ok([CallerMemberName] string context = null) => new CompositeResult(context);

        protected CompositeResult(string context)
            : base(context)
        {
            results = new List<IResult>();
        }

        public static implicit operator bool(CompositeResult result)
        {
            return !result.Issues.Any()
                   && result.Results.All(r => r.IsOk);
        }

        #region IResult

        public new bool IsOk => this;
        public override IReadOnlyList<Issue> Issues => new List<Issue>(base.issues.Concat(Results.SelectMany(r => r.Issues)));

        #endregion IResult

        public virtual CompositeResult AddResult(Func<IResult> resultFunc, AppendConditions condition = AppendConditions.Always)
        {
            if(condition == AppendConditions.Always || IsOk)
            {
                results.Add(resultFunc());
            }

            return this;
        }

        public virtual CompositeResult AddResult(IResult result, AppendConditions condition = AppendConditions.Always)
        {
            if(condition == AppendConditions.Always || IsOk)
            {
                results.Add(result);
            }

            return this;
        }

        #region Types

        public enum AppendConditions
        {
            WhenOk,
            Always,
        }

        #endregion Types

    }

    public class CompositeResult<T> : CompositeResult, IResult<T>
    {
        public virtual T Value { get; set; }

        public CompositeResult(T value, [CallerMemberName] string context = null)
            : base(context)
        {
            Value = value;
        }

        public static CompositeResult<T> Ok([CallerMemberName] string context = null) => new CompositeResult<T>(default(T), context);
        public static CompositeResult<T> Ok(T value, [CallerMemberName] string context = null) => new CompositeResult<T>(value, context);

        public new CompositeResult<T> AddResult(Func<IResult> resultFunc, AppendConditions condition = AppendConditions.Always)
        {
            if(condition == AppendConditions.Always || IsOk)
            {
                base.AddResult(resultFunc());
            }

            return this;
        }

        public new CompositeResult<T> AddResult(IResult result, AppendConditions condition = AppendConditions.Always)
        {
            if(condition == AppendConditions.Always || IsOk)
            {
                base.AddResult(result);
            }

            return this;
        }
    }
}