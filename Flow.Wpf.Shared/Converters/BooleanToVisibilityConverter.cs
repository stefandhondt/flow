﻿using System;
using System.Globalization;
using System.Windows;

namespace Flow.Wpf.Shared.Converters
{
    public class BooleanToVisibilityConverter : ValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(value is bool boolValue))
                return Visibility.Collapsed;

            return boolValue
                ? Visibility.Visible
                : Visibility.Collapsed;
        }
    }
}