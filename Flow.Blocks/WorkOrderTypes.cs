﻿namespace Flow.Blocks
{
    public enum WorkOrderTypes
    {
        Undefined = 0,
        Truck = 1,
        Train = 2,
        Ship = 3,
        Tank = 4
    }
}