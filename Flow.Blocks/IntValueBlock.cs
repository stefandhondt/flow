﻿using System.Linq;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Blocks
{
    [Setting("Value", "0")]
    public class IntValueBlock : FunctionalBlock
    {
        [Contribution]
        public int Value => int.Parse(Settings.Single(s => s.Key == nameof(Value)).Value);

        public IntValueBlock(ILoggerFactory loggerFactory)
            : base("Int Value", null, loggerFactory)
        {
        }
    }
}