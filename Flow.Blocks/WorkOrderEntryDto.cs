﻿using System;

namespace Flow.Blocks
{
    public class WorkOrderEntryDto
    {
        public string Name { get; set; }
        public WoeStates State { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? CompletionDate { get; set; }

        public WorkOrderEntryDto()
        {
            State = WoeStates.Open;
        }
    }
}