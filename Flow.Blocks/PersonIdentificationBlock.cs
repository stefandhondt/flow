﻿using System;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    [Setting("B1-S1")]
    [Setting("B1-S2long", "blah")]
    [Setting("B1-S3veryveryverylong")]
    public class PersonIdentificationBlock : FunctionalBlock
    {
        [Contribution]
        public string Identification { get; set; }

        [Contribution]
        public DriverDto Person { get; set; }

        public PersonIdentificationBlock(ILoggerFactory loggerFactory)
            : base("Person Identification", null, loggerFactory)
        {
        }

        public PersonIdentificationBlock(string name, ILoggerFactory loggerFactory)
            : base(name ?? "Person Identification", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            Identification = Guid.NewGuid().ToString().Substring(12);
            Person = new DriverDto
            {
                FirstName = "John",
                LastName = "Skeet",
                DateOfBirth = DateTime.Today.AddYears(-40),
                License = "LI-" + Guid.NewGuid().ToString().Substring(12)
            };

            return result;
        }
    }

    [Setting("MandatoryLicenses")]
    public class DriverIdentificationBlock : PersonIdentificationBlock
    {
        [Contribution]
        public string DriversLicense { get; set; }

        public DriverIdentificationBlock(ILoggerFactory loggerFactory)
            : base("Driver Identification", loggerFactory)
        {
        }

        public DriverIdentificationBlock(string name, ILoggerFactory loggerFactory)
            : base(name ?? "Driver Identification", loggerFactory)
        {
        }
    }

    [Setting("MandatorySkills")]
    [Setting("B1-S2long", "blah skilled")]
    public class SkilledDriverIdentificationBlock : DriverIdentificationBlock
    {
        public SkilledDriverIdentificationBlock(ILoggerFactory loggerFactory)
            : base("Skilled Driver Identification", loggerFactory)
        {
        }
    }
}