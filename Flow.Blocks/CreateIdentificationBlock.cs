﻿using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Blocks
{
    public class CreateIdentificationBlock : FunctionalBlock
    {
        [Requirement]
        public IdentificationTypes IdentificationType { get; set; }

        [Contribution]
        public IdentificationDto Identification { get; set; }

        public CreateIdentificationBlock(ILoggerFactory loggerFactory)
            : base("Create Identification", null, loggerFactory)
        {
        }
    }
}