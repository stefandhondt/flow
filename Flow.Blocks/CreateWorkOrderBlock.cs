﻿using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;

namespace Flow.Blocks
{
    public class CreateWorkOrderBlock : FunctionalBlock
    {
        [Requirement]
        public string Reference { get; set; }

        [Requirement]
        public WorkOrderTypes WorkOrderType { get; set; }

        [Contribution]
        public WorkOrderDto WorkOrder { get; set; }

        public CreateWorkOrderBlock(ILoggerFactory loggerFactory)
            : base("Create Work Order", null, loggerFactory)
        {
        }
    }
}