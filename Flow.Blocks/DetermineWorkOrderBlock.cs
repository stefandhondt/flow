﻿using System;
using Agidens.Logging;
using Flow.Core;
using Flow.Core.Attributes;
using Flow.Core.Result;

namespace Flow.Blocks
{
    public class DetermineWorkOrderBlock : FunctionalBlock
    {
        [Requirement]
        public string Identification { get; set; }

        [Contribution]
        public WorkOrderDto WorkOrder { get; set; }

        [Requirement]
        public PersonDto Person { get; set; }

        public DetermineWorkOrderBlock(ILoggerFactory loggerFactory)
            : base("Determine Work Order", null, loggerFactory)
        {
        }

        public override IResult<IBlock> Activate()
        {
            var result = base.Activate();
            WorkOrder = new WorkOrderDto
            {
                Reference = "WO-" + Guid.NewGuid().ToString().Substring(0, 8),
                Created = DateTime.Now
            };

            return result;
        }
    }
}