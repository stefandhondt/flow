﻿using System;
using System.Collections.Generic;

namespace Flow.Blocks
{
    public class WorkOrderDto
    {
        public string Reference { get; set; }
        public DateTime Created { get; set; }
        public List<WorkOrderEntryDto> Woes { get; set; }

        public WorkOrderDto()
        {
            Woes = new List<WorkOrderEntryDto>();
        }

        public override string ToString()
        {
            return $"{Reference} ({Created:O})";
        }
    }
}