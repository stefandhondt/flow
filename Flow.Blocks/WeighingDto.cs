﻿using System;

namespace Flow.Blocks
{
    public class WeighingDto
    {
        public decimal WeightInKg { get; set; }
        public string Weighbridge { get; set; }
        public string WeighingToken { get; set; }
        public DateTime Timestamp { get; set; }

        public override string ToString()
        {
            return $"{WeightInKg}kg ({Weighbridge} - {WeighingToken} - {Timestamp:O})";
        }
    }
}