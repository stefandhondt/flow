using System;
using System.Collections.Generic;
using System.Reflection;

namespace Flow.Kiosk.Shell.Resolvers
{
    public interface IViewModelResolver
    {
        IEnumerable<Type> Resolve(IEnumerable<Assembly> assemblies);
    }
}