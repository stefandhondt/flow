﻿using System.Globalization;

namespace Flow.Kiosk.Shell
{
    public interface IShellSettings
    {
        CultureInfo DefaultCulture { get; }
        string FlowFilename { get; set; }
    }
}