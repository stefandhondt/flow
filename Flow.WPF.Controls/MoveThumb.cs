﻿using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Flow.WPF.Controls
{
    public class MoveThumb : Thumb
    {
        public MoveThumb()
        {
            DragDelta += new DragDeltaEventHandler(MoveThumb_DragDelta);
        }

        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if(!(DataContext is Control item))
                return;

            if(!(item.Parent is Canvas canvasInstance))
                return;

            var left = Canvas.GetLeft(item);
            var top = Canvas.GetTop(item);
            var newLeft = left + e.HorizontalChange;
            var newTop = top + e.VerticalChange;

            if(newLeft < 0)
                newLeft = 0;
            else if(newLeft > canvasInstance.ActualWidth - item.ActualWidth)
                newLeft = canvasInstance.ActualWidth - item.ActualWidth;

            if(newTop < 0)
                newTop = 0;
            else if(newTop > canvasInstance.ActualHeight - item.ActualHeight)
                newTop = canvasInstance.ActualHeight - item.ActualHeight;

            Canvas.SetLeft(item, newLeft);
            Canvas.SetTop(item, newTop);
        }
    }
}