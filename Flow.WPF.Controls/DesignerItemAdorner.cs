﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Flow.WPF.Controls
{
    public class DesignerItemAdorner : Adorner
    {
        private VisualCollection visuals;
        //private DesignerItemAdornerChrome chrome;

        protected override int VisualChildrenCount => visuals.Count;

        public DesignerItemAdorner(ContentControl designerItem)
            : base(designerItem)
        {
           //chrome = new DesignerItemAdornerChrome();
           //chrome.DataContext = designerItem;
           visuals = new VisualCollection(this);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            //chrome.Arrange(new Rect(arrangeBounds));
            return arrangeBounds;
        }

        protected override Visual GetVisualChild(int index)
        {
            return visuals[index];
        }
    }
}